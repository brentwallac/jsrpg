"use strict";
// Character is a prototypical object. 
var Character = function(name,str,dex,endr,catchphrases) {
	this.name = name;
	this.str = str; // Strength
	this.dex = dex; // dexterity
	this.endr = endr; // Endurance
	this.hitpoints = 0
	this.max_hp = function(str,endr) {
		console.log((str * endr) * 10);
		return (str * endr) * 10;
	}
	this.catchphrases = catchphrases;
	this.hitpoints = this.set_hitpoints(this.str,this.endr);
};

Character.prototype.say_catchphrase = function() {
	var max_length = this.catchphrases.length;
	var x = Math.floor(Math.random() * (max_length - 0)) + 0;
	
	return this.catchphrases[x];
};

Character.prototype.get_hitpoints = function() {
	return this.hitpoints;
};
Character.prototype.set_hitpoints = function(str,endr) {
	this.hitpoints = this.max_hp(str,endr);
}
Character.prototype.print_hitpoints = function() {
	return this.hitpoints;
}
Character.prototype.take_damage = function(value) {
	this.hitpoints -= value;
};



