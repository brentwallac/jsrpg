/**
* Generates a new enemy based off the Character prototype
*
*
**/

var Enemy = (function() {
	
	console.log("Starting enemy generator");
	
	function generate_skeleton() { 
		
		function Skeleton() { 
		
			var catch_phrase = ["GONNA GET YOU!","A-HA!","RARR"];
			Character.call(this,"Skeleton",3,2,2,catch_phrase); // Starts the constructor
		}
		
		Skeleton.prototype = Object.create(Character.prototype); // Copy Prototype
		Skeleton.prototype.constructor = Skeleton;
		Skeleton.prototype.on_death =  function() {
			return this.name + " has died.";
		}
		
		return new Skeleton();
	
	}

	function generate_boar() {
		
		function Boar() {
			var catch_phrase = ["OOOINK","Oink"];
			Boar.call(this,"Boar",1,1,2,catch_phrase);
		}
		Boar.prototype = Object.create(Character.prototype); // Copy Prototype
		Boar.prototype.constructor = Boar;
		
		Boar.prototype.on_death = function() {
			return this.name + " has died.";
		}

		return new Boar();
	}
	return {
		spawn_boar: function() {
			return generate_boar();
		},
		spawn_skeleton: function() {
			return generate_skeleton();
		}

	}	

})();