 var Message = (function() {

	var _msgArea = document.getElementById('messagebox');
	function clear_content() {
		_msgArea.innerHTML = "";
	}	
	function display_content(message) {
		_msgArea.innerHTML += '<p>' + message + '</p>';
	}
	return {
		set_message: function(message) {
			// Does check here.
			try {
	
				if (typeof message === "string" && message.length > 0) {
					// Send to the display content
					display_content(message);
				}else{
					
					throw new Error("Message only accepts string.");
				}
			} catch (e) {
				console.log("Oops. " + e);
			}	
		}
	}

})();